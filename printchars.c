
#include "csapp.h"
#include "sbuf.h"

typedef enum {false, true} bool;
void *lectura(void *args);
void *escritura(void *args);
sbuf_t buffer;
bool volatile isActive = true;

int main(int argc, char **argv)
{
	char *filename;
	int fd;
	struct stat fileStat;
	pthread_t cons, prod;

	if (argc != 2) {
		printf("usage: %s <filename>\n", argv[0]);
		exit(0);
	}
	filename = argv[1];


	if (stat(filename, &fileStat)<0){	
		printf("Archivo %s no fue encontrado\n",filename);
	}
	else{
		printf("Abriendo archivo %s...\n",filename);
		
		Pthread_create(&prod, NULL, escritura, filename);
		Pthread_create(&cons, NULL, lectura, NULL);

		Pthread_join(prod,NULL);
		Pthread_join(cons,NULL);

	}
	
	return 0;
}

void *lectura(void *args){
	char caracter;
	while(isActive){
		sleep(1);
		caracter = sbuf_remove(&buffer);
		printf("%c\n",caracter);
	}
	exit(0);
}

void *escritura(void *args){
	char *filename;
	filename = (char *) args;
	int fd = Open(filename, O_RDONLY, 0);
	char caracter;
	sbuf_init(&buffer,10);
	while(Read(fd,&caracter,1)){
		sleep(1);
		usleep(50);
		sbuf_insert(&buffer,caracter);
	}			
	Close(fd);
	isActive = false;
}